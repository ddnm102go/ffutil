package ffutil_test

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ddnm102go/ffutil"
)

func TestFiles(t *testing.T) {
	files, err := ffutil.Files("test/", false, ".bak")
	assert.Nil(t, err)

	file, err := os.OpenFile("test.log", os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	defer file.Close()
	// t.Logf("%v", files)
	for _, f := range files {
		fmt.Println(f.Name())
		fmt.Fprintln(file, f.Name())
	}

	fmt.Println("-----------------------------------")
	fmt.Fprintln(file, "-----------------------------------")
	ffutil.SortFileNameAscend(files)
	for _, f := range files {
		fmt.Println(f.Name())

		fmt.Fprintln(file, f.Name())
	}
	fmt.Println("-----------------------------------")
	fmt.Fprintln(file, "-----------------------------------")
	ffutil.SortFileNameDescend(files)
	for _, f := range files {
		fmt.Println(f.Name())
		fmt.Fprintln(file, f.Name())
	}

	ffutil.PrintFiles(files)
}
